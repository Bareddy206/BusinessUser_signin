const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const ejs = require('ejs');
const path = require('path');
const cookieParser = require('cookie-parser');
require('dotenv').config();
const app = express();

const router = express.Router();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(cookieParser());


app.use('/', router);
router.use((err, req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.locals.error = process.env.NODE_ENV === 'production' ? err : {};
  console.log("suma: " + process.env.NODE_ENV);
  next();
});

const users = require('./routes/routes');
app.use('/', users);

module.exports = app;