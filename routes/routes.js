const router = require('express').Router();
const seneca = require('seneca')();

//pages display
router.get('/', (req, res) => {
	res.render('signin');
});
router.get('/signup', (req, res) => {
	res.render('signup');

});
router.get('/verify', (req, res) => {
	res.render('verify');

});
router.get('/anim', (req, res) => {
	res.render('anim');

});
router.get('/forgotpassword', (req, res) => {
	res.render('reset_pass_mail');
});
router.get('/changepwd', (req, res) => {
	var email = req.query.email;
	console.log("email::"+email);
	res.render('choose_password', {
		email: email
	});
});



//post data to microservices with seneca client

//client request to microservice
router.post('/signin', (req, res) => {
	console.log("bodyparams::" + JSON.stringify(req.body));
	const email = req.body.email;
	const password = req.body.password;
	seneca.client({
			host: 'ec2-13-232-92-165.ap-south-1.compute.amazonaws.com',
			//host:'localhost',
			port: 2554,
			timeout$: 99999
		})
		.act({
			"role": "user",
			"cmd": "signin",
			"email": email,
			"password": password
		}, (err, reply) => {
			if (err) {
				console.log("Error while login::" + err);
				res.send({
					status: "error",
					message: "Client request timeout!"
				});
			} else {
				//console.log("signin::" + JSON.stringify(reply));
				res.send(reply);
			}
		});
});

//client request to microservice
router.post('/signup', (req, res) => {
	const bodyParams = req.body;
	const storeName = bodyParams.storeName;
	const userEmail = bodyParams.email;
	const rootUrl = `${req.protocol}://${req.get('host')}`; //equates to http :// localhost or ec2hostname
	const password = bodyParams.password;
	const cpassword = bodyParams.cpassword;
	const username = bodyParams.username;
	seneca.client({
			host: 'ec2-13-232-92-165.ap-south-1.compute.amazonaws.com',
			//host:'localhost',
			port: 2550,
			timeout$: 99999
		})
		.act({
			"role": "user",
			"cmd": "signup",
			"username": username,
			"email": userEmail,
			"password": password,
			"cpassword": cpassword,
			"storeName": storeName,
			"rootUrl": rootUrl
		}, (err, reply) => {
			if (err) {
				console.error("error while inserting an user:" + err);
				res.send({
					status: 'error',
					message: 'Client request timeout!'
				});
			} else {
				//console.log("user inseted:" + JSON.stringify(reply));
				res.send(reply);
			}
		});
});


//client request to microservice
//Signup Verification
router.get('/signupverification', (req, res) => {
	const queryParams = req.query;
	console.log("queryParams::" + JSON.stringify(queryParams));
	const userEmail = queryParams.email;
	const rootUrl = `${req.protocol}://${req.get('host')}`;
	const storeName = queryParams.storeName;
	const verification_code = queryParams.verification_code;
	const pwd = queryParams.pwd;
	seneca.client({
			host: 'ec2-13-232-92-165.ap-south-1.compute.amazonaws.com',
			//host:'localhost',
			port: 2550,
			timeout$: 99999
		})
		.act({
			"role": "user",
			"cmd": "signupverification",
			"userEmail": userEmail,
			"verification_code": verification_code,
			"storeName": storeName,
			"pwd": pwd,
			"rootUrl": rootUrl
		}, (err, reply) => {
			if (err) {
				return console.error("error while doing signupverification :" + err);
			} else {
				//console.log("in signupverification::" + JSON.stringify(reply));
				if (reply.status === "success")
					res.render('anim');
				else if (reply.status === "verified") {
					res.redirect('/');

				} else {
					res.send(reply);
				}
			}
		});
});


//client request to microservice to check email
router.post('/emailcheck', (req, res) => {
	var email = req.body.email;
	seneca.client({
			host: 'ec2-13-232-92-165.ap-south-1.compute.amazonaws.com',
			//host:'localhost',
			port: 2550,
			timeout$: 99999
		})
		.act({
			"role": "user",
			"cmd": "emailcheck",
			"email": email
		}, (err, reply) => {

			if (err) {
				res.send({
					status: "error",
					message: "error while checking email!"
				});
				return;
			} else {
				console.log("emailCheck::"+JSON.stringify(reply));
				res.send(reply);
			}
		})


});

//client request to microservice to check storeName
router.post('/storecheck', (req, res) => {
	var storeName = req.body.storeName;
	seneca.client({
			host: 'ec2-13-232-92-165.ap-south-1.compute.amazonaws.com',
			//host:'localhost',
			port: 2550,
			timeout$: 99999
		})
		.act({
			"role": "user",
			"cmd": "storecheck",
			"storeName": storeName
		}, (err, reply) => {
			if (err) {
				console.log(err);
				res.send({
					status: "error",
					message: "error while checking storeName!"
				});
				return;
			} else {
				res.send(reply);
			}
		})
});


//client request to microservice to send mail
router.post('/forgotpwd', (req, res) => {
	const email = req.body.email;
	const rootUrl = `${req.protocol}://${req.get('host')}`;
	seneca.client({
			host: 'ec2-13-232-92-165.ap-south-1.compute.amazonaws.com',
			//host:'localhost',
			port: 2552,
			timeout$: 99999
		})
		.act({
			"role": "user",
			"cmd": "forgotpwd",
			"email": email,
			"rootUrl":rootUrl
		}, (err, reply) => {
			if (err) {
				console.log(err);
				res.send({
					status: "error",
					message: "error while sending email!"
				});
				return;
			} else {
				console.log("forgot password::"+JSON.stringify(reply));
				res.send(reply);
			}
		})

});


//client request to microservice to update the password
router.post('/updatepwd', (req, res) => {
	const bodyParams = req.body;
	console.log("updatePassword::"+JSON.stringify(bodyParams));
	var email = bodyParams.email;
	var newpassword = bodyParams.newpassword;
	seneca.client({
			host: 'ec2-13-232-92-165.ap-south-1.compute.amazonaws.com',
			//host:'localhost',
			port: 2558,
			timeout$: 99999
		})
		.act({
			"role": "user",
			"cmd": "updatepwd",
			"email": email,
			"newpassword": newpassword
		}, (err, reply) => {
			if (err) {
				console.log(err);
				res.send({
					status: "error",
					message: "error while updating password!"
				});
				return;
			} else {
				res.send(reply);
			}
		})

});



module.exports = router;